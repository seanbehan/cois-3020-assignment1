come up with testing and planning first
draw out how you want your subway system to look

Use breadth-first search
use a queue, instead of a stack to hold data

user will say: find me the shortest distance from x to y

also have to find out which subway stations break the graph into
two, three, or four parts.

to do this: use depth first search

articulation point is the point that splits the graph into two
or more parts

you need to find all the articulation points
