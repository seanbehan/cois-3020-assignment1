using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIS_3020_assignment0
{
    public class Edge<T>
    {
        public Vertex<T> AdjVertex { get; set; }
        public string Colour { get; set; }

        public Edge(Vertex<T> vertex, string colour)
        {
            AdjVertex = vertex;
            Colour = colour;
        }
    }

    //---------------------------------------------------------------------------------------------

    public class Vertex<T>
    {             
        public T Name { get; set; }              // Vertex name
        public bool Visited { get; set; }
        public List<Edge<T>> E { get; set; }     // List of adjacency vertices

        public Vertex(T name)
        {
            Name = name;
            Visited = false;
            E = new List<Edge<T>>();
        }

        // FindEdge
        // Returns the index of the given adjacent vertex in E; otherwise returns -1
        // Time complexity: O(n) where n is the number of vertices

        public int FindEdge(T name)
        {
            int i;
            for (i = 0; i < E.Count; i++)
            {
                if (E[i].AdjVertex.Name.Equals(name))
                    return i;
            }
            return -1;
        }
    }

    //---------------------------------------------------------------------------------------------

    public interface IDirectedGraph<T>
    {
        void AddVertex(T name);
        void RemoveVertex(T name);
        void AddEdge(T name1, T name2, string colour);
        void RemoveEdge(T name1, T name2);
    }

    //---------------------------------------------------------------------------------------------

    public class DirectedGraph<T> : IDirectedGraph<T>
    {
        private List<Vertex<T>> V;

        public DirectedGraph()
        {
            V = new List<Vertex<T>>();
        }

        // FindVertex
        // Returns the index of the given vertex (if found); otherwise returns -1
        // Time complexity: O(n)

        public int FindVertex(T name)
        {
            int i;

            for (i = 0; i < V.Count; i++)
            {
                if (V[i].Name.Equals(name))
                    return i;
            }
            return -1;
        }

        // AddVertex
        // Adds the given vertex to the graph
        // Note: Duplicate vertices are not added
        // Time complexity: O(n) due to FindVertex

        public void AddVertex(T name)
        {
            if (FindVertex(name) == -1)
            {
                Vertex<T> v = new Vertex<T>(name);
                V.Add(v);
            }
        }

        // RemoveVertex
        // Removes the given vertex and all incident edges from the graph
        // Note:  Nothing is done if the vertex does not exist
        // Time complexity: O(max(n,m)) where m is the number of edges

        public void RemoveVertex(T name)
        {
            int i, j, k;
            if ((i = FindVertex(name)) > -1)
            {
                for (j = 0; j < V.Count; j++)
                {
                    for (k = 0; k < V[j].E.Count; k++)
                        if (V[j].E[k].AdjVertex.Name.Equals(name))   // Incident edge
                        {  
                            V[j].E.RemoveAt(k);
                            break;  // Since there are no duplicate edges
                        }
                }
                V.RemoveAt(i);
            }
        }

        // AddEdge
        // Adds the given edge (name1, name2) to the graph
        // Notes: Duplicate edges are not added
        //        By default, the colour of the edge is ""
        // Time complexity: O(n)

        public void AddEdge(T name1, T name2, string colour = "")
        {
            int i, j;
            Edge<T> e;
            
            // Do the vertices exist?
            if ((i = FindVertex(name1)) > -1 && (j = FindVertex(name2)) > -1)
            {
                // Does the edge not already exist?
                if (V[i].FindEdge(name2) == -1)
                {
                    e = new Edge<T>(V[j], colour);
                    V[i].E.Add(e);
                }
            }
        }

        // RemoveEdge
        // Removes the given edge (name1, name2) from the graph
        // Note: Nothing is done if the edge does not exist
        // Time complexity: O(n)

        public void RemoveEdge(T name1, T name2)
        {
            int i, j;
            if ((i = FindVertex(name1)) > -1 && (j = V[i].FindEdge(name2)) > -1)
                V[i].E.RemoveAt(j);
        }

        // Depth-First Search
        // Performs a depth-first search (with re-start)
        // Time complexity: O(max,(n,m))

        public void DepthFirstSearch()
        {
            int i;

            for (i = 0; i < V.Count; i++)     // Set all vertices as unvisited
                V[i].Visited = false;

            for (i = 0; i < V.Count; i++)
                if (!V[i].Visited)                  // (Re)start with vertex i
                {
                    DepthFirstSearch(V[i]);
                    Console.WriteLine();
                }
        }

        private void DepthFirstSearch(Vertex<T> v)
        {
            int j;
            Vertex<T> w;

            v.Visited = true;    // Output vertex when marked as visited
            Console.WriteLine(v.Name);

            for (j = 0; j < v.E.Count; j++)       // Visit next adjacent vertex
            {
                w = v.E[j].AdjVertex;  // Find index of adjacent vertex in V
                if (!w.Visited)
                    DepthFirstSearch(w);
            }
        }

        // Breadth-First Search
        // Performs a breadth-first search (with re-start)
        // Time Complexity: O(max(n,m))

        public void BreadthFirstSearch()
        {
            int i;

            for (i = 0; i < V.Count; i++)
                V[i].Visited = false;              // Set all vertices as unvisited

            for (i = 0; i < V.Count; i++)
                if (!V[i].Visited)                  // (Re)start with vertex i
                {
                    BreadthFirstSearch(V[i]);
                    Console.WriteLine();
                }
        }

        private void BreadthFirstSearch(Vertex<T> v)
        {
            int j;
            Vertex<T> w;
            Queue<Vertex<T>> Q = new Queue<Vertex<T>>();

            v.Visited = true;        // Mark vertex as visited when placed in the queue
            Q.Enqueue(v);            // Why? 

            while (Q.Count != 0)
            {
                v = Q.Dequeue();     // Output vertex when removed from the queue
                Console.WriteLine(v.Name);

                for (j = 0; j < v.E.Count; j++)    // Enqueue unvisited adjacent vertices
                {
                    w = v.E[j].AdjVertex;
                    if (!w.Visited)
                    {
                        w.Visited = true;          // Mark vertex as visited
                        Q.Enqueue(w);
                    }
                }
            }
        }

        // PrintVertices
        // Prints out all vertices of a graph
        // Time complexity: O(n)

        public void PrintVertices()
        {
            for (int i = 0; i < V.Count; i++)
                Console.WriteLine(V[i].Name);
            Console.ReadLine();
        }

        // PrintEdges
        // Prints out all edges of the graph
        // Time complexity: O(m)

        public void PrintEdges()
        {
            int i, j;
            for (i = 0; i < V.Count; i++)
                for (j = 0; j < V[i].E.Count; j++)
                        Console.WriteLine("(" + V[i].Name + ", " + V[i].E[j].AdjVertex.Name + ", " + V[i].E[j].Colour + ")");
        }

        public Vertex<T> GetVertex(int number) {
            return V[number];
        }

        public Vertex<T> vertexInQMinDist(List<Vertex<T>> Q, Dictionary<Vertex<T>, int> distance) {
            Vertex<T> min = Q[0];
            foreach (Vertex<T> v in Q) {
                if (distance[v] < distance[min]) {
                    min = v;
                }
            }
            return min;
        }

        public List<String> GetColours(List<Vertex<T>> S, Dictionary<Vertex<T>, Vertex<T>> prev) {
            var colours = new List<String>();
            // create a new list to hold our colours

            foreach (Vertex<T> v in S) {
            // for each vertex
                foreach (Edge<T> e in v.E) {
                // for each vertex edge
                    if (e.AdjVertex == prev[v]) {
                    // if the edge's adjacent vertex is the previous vertex
                        if (colours.LastOrDefault() != e.Colour) {
                        // and if the last colour wasn't the same
                            colours.Add(e.Colour);
                            // add it to our list of colours
                        }
                    }
                }
            }
            return colours; // return our list of colours
        }

        public Tuple<Dictionary<Vertex<T>, int>, Dictionary<Vertex<T>, Vertex<T>>> Dijkstra(Vertex<T> source, Vertex<T> target) {
            // used the wikipedia page here to help me understand and write
            // this algorithm
            // https://en.wikipedia.org/wiki/Dijkstra's_algorithm
            //
            // some of the distance things are unneccesary for what we're
            // doing, but it works with them anyways so I left them in

            var prev = new Dictionary<Vertex<T>, Vertex<T>>();
            var dist = new Dictionary<Vertex<T>, int>();
            List<Vertex<T>> Q = new List<Vertex<T>>();
            // initialization

            foreach (Vertex<T> v in V) { // for each vertex
                if (v == source) { // set the dist of source to 0
                    dist[v] = 0; // set to 0
                }
                else {
                    dist[v] = int.MaxValue; // set to infinity
                }
                prev[v] = null; // set the previous node to null
                Q.Add(v); // add to our list
            }

            while (Q.Count != 0) { // while the list still has items in it
                Vertex<T> u = vertexInQMinDist(Q, dist);
                // find the vertex with the minimum distance

                Q.Remove(u);
                // remove it

                if (u == target) { // if we found the vertex we wanted
                    break; // end the traversal
                }

                foreach (Edge<T> e in u.E) { // for every edge
                    Vertex<T> v = e.AdjVertex; // get the edge vertex
                    int alt = dist[u]; // + length(u, v)
                    if (alt < dist[v]) { // if we found a shorter path
                        dist[v] = alt;
                        // set the distance to the distance of the alternate path

                        prev[v] = u;
                        // save u as our previous vertex
                    }
                }
            }
            return Tuple.Create(dist, prev);
            // return both our distance and previous vertices
        }

        public List<T> GetArticulationPoints2(List<T> articulationPoints, Dictionary<Vertex<T>, int> depth, Dictionary<Vertex<T>, int> low, Dictionary<Vertex<T>, Vertex<T>> parent, Vertex<T> i = null, int d = 0) {
            // used the wikipedia page here to help me understand and write
            // this algorithm
            // https://en.wikipedia.org/wiki/Biconnected_component

            int childCount = 0;
            bool isArticulation = false;
            // initialization values

            i.Visited = true;
            // we just visited it, so set it

            depth[i] = d;
            // set the depth

            low[i] = depth[i];
            // set the low to the same as the current depth

            foreach (Edge<T> e in i.E) { // for each edge
                Vertex<T> ni = e.AdjVertex; // get the adjacent vertex of that edge
                if (!ni.Visited) {
                    parent[ni] = i;

                    GetArticulationPoints2(articulationPoints, depth, low, parent, ni, d + 1);
                    // recursive call

                    childCount++;
                    // increment the children

                    if (low[ni] >= depth[i]) {
                        isArticulation = true;
                    }
                    // if the depth of i is smaller than the low of the
                    // adjacent vertex, it must be an articulation point
                    // because this is where the graph splits

                    low[i] = Math.Min(low[i], low[ni]);
                    // set low to the lesser of the two
                }

                else if (ni != parent[i]) {
                    low[i] = Math.Min(low[i], depth[ni]);
                } // as long as the adjacent node isn't the parent, set it to
                  // the lesser of the two
            }
            if ((parent[i] != null && isArticulation) || (parent[i] == null && childCount > 1)) {
                articulationPoints.Add(i.Name);
            }
            // if the parent is not null and the point was an artiulation point
            // or if the parent is null and it has more than one child then add
            // it to our list of articulation points

            return articulationPoints;
            // return the list of articulation points, the recursive call saves it
        }

        public List<T> GetArticulationPoints(Vertex<T> i = null, int d = 0) {
            var articulationPoints = new List<T>();

            if (i == null) {
                i = V[0];
            } // if the user didn't specify i, set it to vertex 0

            var low = new Dictionary<Vertex<T>, int>();
            var depth = new Dictionary<Vertex<T>, int>();
            var parent = new Dictionary<Vertex<T>, Vertex<T>>();
            // dicts to hold data

            foreach (Vertex<T> v in V) {
                parent[v] = null;
            } // set all parents to null

            foreach (Vertex<T> v in V) {
                v.Visited = false;
            } // set all points to unvisited

            return GetArticulationPoints2(articulationPoints, depth, low, parent, i, d);
        }

        public List<Edge<T>> GetEdges(List<Vertex<T>> S, Dictionary<Vertex<T>, Vertex<T>> prev) {
            var results = new List<Edge<T>>();
            foreach (Vertex<T> v in S) { // for each vertex
                foreach (Edge<T> e in v.E) { // for each vertex edge
                    if (e.AdjVertex == prev[v]) { // if the edge's adjacent vertex is the same as the previous vertex of v
                        results.Add(e); // add the edge to the list of edges we return
                    }
                }
            }
            return results; // return the results
        }
    }
}
