using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace COIS_3020_assignment0 {
    public class SubwayStation {
        public int number;
        public string name;
        public SubwayStation(int number, string name) {
            this.number = number;
            this.name = name;
        }
        public override string ToString() {
            return String.Format("[{0}, {1}]", this.name, this.number);
        }
    }

    public class Link {
        public SubwayStation from_;
        public SubwayStation to;
        public Link(SubwayStation from_, SubwayStation to) {
            this.from_ = from_;
            this.to = to;
        }
    }

    class SubwayMap {
        public DirectedGraph<SubwayStation> map;
        private string[] colours;
        private Random rng;

        public void InsertStation(SubwayStation station) {
            if (map.FindVertex(station) == -1) {
                this.map.AddVertex(station);
                // insert vertex into map
            }
            // check if station is not in map
        }
        // insert a subway station

        private void InsertStations(SubwayStation[] stations) {
            foreach (SubwayStation station in stations) {
                this.map.AddVertex(station);
            }
        }
        // take a list of stations and insert each one

        public void InsertLink(SubwayStation from_, SubwayStation to) {
            int colour1 = rng.Next(1, this.colours.Length);
            int colour2 = rng.Next(1, this.colours.Length);
            while (colour1 == colour2) {
                colour2 = rng.Next(1, this.colours.Length);
            } // make sure we don't chose the same colour

            this.map.AddEdge(from_, to, this.colours[colour1]);
            this.map.AddEdge(to, from_, this.colours[colour2]);
        }
        // add links in both directions

        private void InsertLinks(Link[] links) {
            foreach (Link link in links) {
                InsertLink(link.from_, link.to);
            }
        }
        // take a list of links to insert and insert each one

        public void DeleteLink(SubwayStation from_, SubwayStation to) {
            this.map.RemoveEdge(from_, to);
            this.map.RemoveEdge(to, from_);
        }
        // delete link in both directions

        public List<String> FastestRoute (int from_, int to) {
            var S = new List<Vertex<SubwayStation>>();
            // stack

            Vertex<SubwayStation> v = map.GetVertex(from_);
            // source

            Vertex<SubwayStation> u = map.GetVertex(to);
            // target

            var dist_prev = map.Dijkstra(v, u);
            Dictionary<Vertex<SubwayStation>, int> dist = dist_prev.Item1;
            Dictionary<Vertex<SubwayStation>, Vertex<SubwayStation>> prev = dist_prev.Item2;

            if ((prev[u] != null) || (u == v)) {
                while (u != null) {
                    S.Insert(0, u);
                    // push vertex onto our stack S

                    u = prev[u];
                    // traverse backwards from target to source
                }
                // construct the shortest path with our stack S
            }
            // only do stuff if the vertex is reachable
            
            List<Edge<SubwayStation>> edges = map.GetEdges(S, prev);

            // this block of code doesn't necessarily need to be here
            // this is a test
            foreach (Edge<SubwayStation> e in edges) {
                Vertex<SubwayStation> tto = e.AdjVertex;
                Vertex<SubwayStation> ffrom = prev[tto];
                String fffrom = "";
                if (ffrom != null) {
                    fffrom = ffrom.Name.name;
                }
                String colour = e.Colour;

                Console.WriteLine("EDGE from: {0}, to: {1}, colour: {2}", fffrom, tto.Name, colour);
            }

            List<String> colours = map.GetColours(S, prev);
            // get the list of colours

            return colours;
        }
        // use dijkstra's algorithm to compute the shortest path
        // then get the colours using the results and return them
        
        public List<SubwayStation> CriticalStations() {
            return this.map.GetArticulationPoints();
        }

        public SubwayMap(SubwayStation[] stations, Link[] links, string[] colours) {
            this.map = new DirectedGraph<SubwayStation>();

            this.colours = colours;
            // holds the colours our map will have

            this.rng = new Random();
            // for choosing random colours

            InsertStations(stations);
            InsertLinks(links);
        }
        // create new map with the given stations and links
    }
}
