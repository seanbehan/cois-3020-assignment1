/* Assignment 0 
 * Sean Behan
 * 0599444
 */

using System;
using System.Collections.Generic;

namespace COIS_3020_assignment0 {
    class Program {
        static void Main(string[] args) {
            SubwayStation[] stations = new[] {
                new SubwayStation(0, "Toronto"),
                new SubwayStation(1, "New York"),
                new SubwayStation(2, "London"),
                new SubwayStation(3, "San Fransisco"),
                new SubwayStation(4, "Ottawa"),
                new SubwayStation(5, "Calgary"),
            };
            // define our stations

            Link[] links = new[] {
                new Link(stations[0], stations[1]),
                new Link(stations[0], stations[2]),
                new Link(stations[1], stations[0]),
                new Link(stations[1], stations[2]),
                new Link(stations[2], stations[3]),
                new Link(stations[2], stations[4]),
                new Link(stations[3], stations[1]),
                new Link(stations[4], stations[0]),
                new Link(stations[4], stations[3]),
                new Link(stations[4], stations[5]),
            };
            // create our map

            string[] colours = new[] {
                "red",
                "blue",
                "green",
                "orange",
                "purple",
                "yellow",
            };
            // list of colours we'll use for our links

            SubwayMap map = new SubwayMap(stations, links, colours);

            map.map.PrintVertices();
            map.map.PrintEdges();
            // this is here so you can see that it's working correctly

            List<String> c = map.FastestRoute(2, 5);
            foreach (String colour in c) {
                Console.WriteLine(colour);
            }
            // yeehaw we got the shortest path
            
            List<SubwayStation> critical = map.CriticalStations();
            foreach (SubwayStation s in critical) {
                Console.WriteLine("Articulation Point found: {0}", s);
            }

            Console.WriteLine("---- TESTS ----");
            RunTests(map);
            Console.WriteLine("---- TESTS SUCCEEDED ----");
        }

        // why didn't I write unit tests you might ask?
        // because microsoft hates linux... that's why
        // good luck getting nuget working on linux
        public static void RunTests(SubwayMap map) {
            TestInsertDelete(map);
            TestFastestRoute(map);
            TestCriticalPoints(map);
        }
        private static void TestInsertDelete(SubwayMap map) {
            var station1 = new SubwayStation(10, "Test");
            map.InsertStation(station1);

            var station2 = new SubwayStation(-20, "Test2");
            map.InsertStation(station2);
            // create and insert 2 subway stations

            // var station3 = new SubwayStation("hello", "Test2");
            // this fails at compile time

            map.InsertLink(station1, station2);
            // create a link between them

            map.DeleteLink(station1, station2);
            // delete the link between them

            map.DeleteLink(station1, station2);
            // delete the link between them again that we already deleted
            // this shouldn't (and doesn't) throw an error
        }
        private static void TestFastestRoute(SubwayMap map) {
            map.FastestRoute(1, 5);
            map.FastestRoute(5, 5);
            map.FastestRoute(5, 1);
            // these should all succeed

            /* map.FastestRoute(-1, 10); */
            /* map.FastestRoute(1, 20); */
            // these both fail, the constraints are that the input can't be
            // negative, and must be within the size of the map
        }
        private static void TestCriticalPoints(SubwayMap map) {
            map.CriticalStations();
            // this doesn't take any input, not much more I can test
        }
    }
}
